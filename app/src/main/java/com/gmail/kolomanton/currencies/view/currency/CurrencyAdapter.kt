package com.gmail.kolomanton.currencies.view.currency

import android.annotation.SuppressLint
import android.support.v4.util.SparseArrayCompat
import android.support.v7.util.DiffUtil
import android.support.v7.util.ListUpdateCallback
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.gmail.kolomanton.currencies.entity.AdapterConst
import com.gmail.kolomanton.currencies.entity.Currency
import com.gmail.kolomanton.currencies.view.base.DiffCallback
import com.gmail.kolomanton.currencies.view.base.adapter.ViewType
import com.gmail.kolomanton.currencies.view.base.adapter.ViewTypeDelegateAdapter
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject

class CurrencyAdapter(
        clickListener: (Currency) -> Unit,
        textChangeListener: (Float) -> Unit,
        val scrollOnTop: () -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: MutableList<ViewType> = mutableListOf()
    private var delegateAdapter = SparseArrayCompat<ViewTypeDelegateAdapter>()

    val changeTypeSubject = BehaviorSubject.create<Float>()

    init {
        delegateAdapter.put(
                AdapterConst.Currency.ITEM,
                CurrencyDelegateAdapter(clickListener, textChangeListener, changeTypeSubject)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        delegateAdapter.get(getItemViewType(position))
                .onBindViewHolder(holder, items[position], items.size)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any>) {
        if(payloads.isNotEmpty()) {
            delegateAdapter.get(getItemViewType(position))
                    .onUpdateViewHolder(holder, items[position], payloads)
        } else {
            super.onBindViewHolder(holder,position, payloads)
        }
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        delegateAdapter.get(getItemViewType(holder.adapterPosition)).onViewRecycled(holder)
        super.onViewRecycled(holder)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            delegateAdapter.get(viewType).onCreateViewHolder(parent)

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int) = items[position].getViewType()

    @SuppressLint("CheckResult")
    fun setData(currencies: List<Currency>) {
        val oldData = items.toList()

        items.clear()
        items.addAll(currencies)

        Observable.just(
                DiffUtil.calculateDiff(
                        DiffCallback(items as List<Currency>, oldData as List<Currency>),
                        true
                ).dispatchUpdatesTo(object : ListUpdateCallback {
                    override fun onChanged(position: Int, count: Int, payload: Any?) {
                        notifyItemRangeChanged(position, count, payload)
                    }

                    override fun onMoved(fromPosition: Int, toPosition: Int) {
                        notifyItemMoved(fromPosition, toPosition)
                        if (toPosition == 0)
                            scrollOnTop.invoke()
                    }

                    override fun onInserted(position: Int, count: Int) {
                        notifyItemRangeInserted(position, count)
                    }

                    override fun onRemoved(position: Int, count: Int) {
                        notifyItemRangeRemoved(position, count)
                    }

                })
        ).subscribeOn(Schedulers.io())

    }
}