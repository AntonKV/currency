package com.gmail.kolomanton.currencies.model.data.storage

interface LastCurrencyHolder {
    var currencyId: String
    var currencyNumber: Float
}