package com.gmail.kolomanton.currencies.model.data.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.gmail.kolomanton.currencies.entity.Currency
import io.reactivex.Flowable

@Dao
interface CurrencyDao {
    @Query("SELECT * FROM currency_table ORDER BY position")
    fun getAll(): Flowable<List<Currency>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(photos: List<Currency>)
}