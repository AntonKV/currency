package com.gmail.kolomanton.currencies.presenter.currency

import com.arellomobile.mvp.InjectViewState
import com.gmail.kolomanton.currencies.entity.Currency
import com.gmail.kolomanton.currencies.model.interactor.CurrencyInteractor
import com.gmail.kolomanton.currencies.presenter.global.BasePresenter
import ru.terrakok.cicerone.Router
import timber.log.Timber
import javax.inject.Inject

@InjectViewState
class CurrencyPresenter @Inject constructor(
        private val interactor: CurrencyInteractor,
        private val router: Router
) : BasePresenter<CurrencyView>() {

    var isSubscribedOnDB = false

    override fun attachView(view: CurrencyView?) {
        super.attachView(view)
        getCurrency()
    }

    private fun getCurrency() {
        interactor.getCurrency()
                .subscribe({
                    if (!isSubscribedOnDB) {
                        subscribeOnDB()
                        isSubscribedOnDB = true
                    }
                }, {
                    if (it != null) {
                        Timber.e(it)
                        viewState.showError(it)
                    }
                }).connect()
        subscribeOnDB()
    }

    private fun subscribeOnDB() {
        interactor.subscribeOnDB()
                .subscribe({
                    viewState.showCurrency(it.isNotEmpty(), it)
                }, {
                    Timber.e(it)
                }).connect()
    }

    fun onClick(currency: Currency) {
        compositeDisposable.clear()
        isSubscribedOnDB = true
        interactor.saveCurrencyId(currency.id)
        interactor.saveMoneyCount(currency.currency)
        getCurrency()
    }

    fun refresh() {
        compositeDisposable.clear()
        isSubscribedOnDB = true
        getCurrency()
    }

    fun textChange(money: Float) {
        interactor.saveMoneyCount(money)
        viewState.multiplyCurrency(money)
    }

    fun onBackPressed() {
        router.exit()
    }
}