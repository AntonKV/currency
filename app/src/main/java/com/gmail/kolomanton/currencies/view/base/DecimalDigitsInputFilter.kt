package com.gmail.kolomanton.currencies.view.base

import android.text.InputFilter
import android.text.Spanned
import java.util.regex.Pattern


class DecimalDigitsInputFilter : InputFilter {

    private val DIGIT_BEFORE_ZERO = 5
    private val DIGITS_AFTER_ZERO = 2

    private val mPattern: Pattern = Pattern
            .compile("[0-9]{0," + (DIGIT_BEFORE_ZERO - 1)
                    + "}+((\\.[0-9]{0," + (DIGITS_AFTER_ZERO - 1)
                    + "})?)||(\\.)?")

    override fun filter(
            source: CharSequence, start: Int, end: Int,
            dest: Spanned, dstart: Int, dend: Int): CharSequence? {
        val matcher = mPattern.matcher(dest)
        return if (!matcher.matches()) "" else null
    }

}