package com.gmail.kolomanton.currencies.model.interactor

import com.gmail.kolomanton.currencies.entity.Currency
import com.gmail.kolomanton.currencies.entity.LatestCurrencies
import com.gmail.kolomanton.currencies.model.repository.CurrencyRepository
import io.reactivex.Flowable
import javax.inject.Inject

class CurrencyInteractor @Inject constructor(
        private val repository: CurrencyRepository
) {
    fun getCurrency(): Flowable<LatestCurrencies> = repository.getCurrency()
            .doOnNext {
                if (it.currencies.isNotEmpty()) {
                    repository.cacheData(it)
                }
            }

    fun saveMoneyCount(money: Float) {
        repository.saveMoneyNumber(money)
    }

    fun saveCurrencyId(id: String) {
        repository.updateCurrencyId(id)
    }

    fun subscribeOnDB(): Flowable<List<Currency>> = repository.getCacheData().map {
        it.forEach { it.currency = it.rate * repository.getMoneyCount() }
        it
    }
}