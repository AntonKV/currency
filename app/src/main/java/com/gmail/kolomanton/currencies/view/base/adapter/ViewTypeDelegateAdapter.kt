package com.gmail.kolomanton.currencies.view.base.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

interface ViewTypeDelegateAdapter {
    fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder

    fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType?, size: Int)

    fun onViewRecycled(holder: RecyclerView.ViewHolder)

    fun onUpdateViewHolder(holder: RecyclerView.ViewHolder,
                           item: ViewType,
                           payloads: MutableList<Any>)
}