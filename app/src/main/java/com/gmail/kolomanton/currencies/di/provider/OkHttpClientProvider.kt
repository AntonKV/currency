package com.gmail.kolomanton.currencies.di.provider

import android.content.Context
import com.gmail.kolomanton.currencies.BuildConfig
import okhttp3.Cache
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import java.net.HttpURLConnection
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Provider

class OkHttpClientProvider @Inject constructor(
        private val context: Context
) : Provider<OkHttpClient> {

    override fun get() = with(OkHttpClient.Builder()) {
        cache(Cache(context.cacheDir, 20 * 1024))

        connectTimeout(30, TimeUnit.SECONDS)
        readTimeout(30, TimeUnit.SECONDS)
//        addInterceptor {
//            val response = it.proceed(it.request())
//            if (response.isSuccessful &&
//                    response.networkResponse() != null &&
//                    response!!.networkResponse()!!.code() == HttpURLConnection.HTTP_NOT_MODIFIED) {
//                // not modified, return empty response
//                response.newBuilder().body(ResponseBody.create(
//                        MediaType.parse("application/json"), "{}")
//                ).build()
//            } else {
//                response
//            }
//        }
        if (BuildConfig.DEBUG) {
            addNetworkInterceptor(
                    HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
            )
        }
        build()
    }
}