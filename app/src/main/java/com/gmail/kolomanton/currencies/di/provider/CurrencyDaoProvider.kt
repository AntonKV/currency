package com.gmail.kolomanton.currencies.di.provider

import com.gmail.kolomanton.currencies.model.data.database.CurrencyDatabase
import com.gmail.kolomanton.currencies.model.data.database.dao.CurrencyDao
import javax.inject.Inject
import javax.inject.Provider

class CurrencyDaoProvider @Inject constructor(
        private val database: CurrencyDatabase
) : Provider<CurrencyDao> {
    override fun get(): CurrencyDao = database.currencyDao()
}