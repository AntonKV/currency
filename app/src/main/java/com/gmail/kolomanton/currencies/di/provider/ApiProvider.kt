package com.gmail.kolomanton.currencies.di.provider

import com.gmail.kolomanton.currencies.BuildConfig
import com.gmail.kolomanton.currencies.model.data.server.CurrenciesApi
import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Provider

class ApiProvider @Inject constructor(
        private val okHttpClient: OkHttpClient,
        private val gson: Gson
) : Provider<CurrenciesApi> {

    override fun get() =
            Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .baseUrl(BuildConfig.CURRENCIES_API)
                    .build()
                    .create(CurrenciesApi::class.java)
}