package com.gmail.kolomanton.currencies.view.base.adapter

interface ViewType {
    fun getViewType(): Int
}