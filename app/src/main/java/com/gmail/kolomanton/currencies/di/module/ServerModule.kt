package com.gmail.kolomanton.currencies.di.module

import com.gmail.kolomanton.currencies.di.provider.ApiProvider
import com.gmail.kolomanton.currencies.di.provider.GsonProvider
import com.gmail.kolomanton.currencies.di.provider.OkHttpClientProvider
import com.gmail.kolomanton.currencies.model.data.server.CurrenciesApi
import com.gmail.kolomanton.currencies.model.data.storage.LastCurrencyHolder
import com.gmail.kolomanton.currencies.model.data.storage.Prefs
import com.gmail.kolomanton.currencies.model.system.AppSchedulers
import com.gmail.kolomanton.currencies.model.system.SchedulersProvider
import com.google.gson.Gson
import okhttp3.OkHttpClient
import toothpick.config.Module

class ServerModule : Module() {
    init {
        //Network
        bind(Gson::class.java).toProvider(GsonProvider::class.java).providesSingletonInScope()
        bind(OkHttpClient::class.java).toProvider(OkHttpClientProvider::class.java).providesSingletonInScope()
        bind(SchedulersProvider::class.java).toInstance(AppSchedulers())
        bind(CurrenciesApi::class.java).toProvider(ApiProvider::class.java).providesSingletonInScope()

        //Prefs
        bind(LastCurrencyHolder::class.java).to(Prefs::class.java).singletonInScope()
    }
}