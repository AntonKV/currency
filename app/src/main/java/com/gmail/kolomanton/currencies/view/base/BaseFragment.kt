package com.gmail.kolomanton.currencies.view.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.gmail.kolomanton.currencies.extention.inflate


abstract class BaseFragment : MvpAppCompatFragment() {
    abstract val layoutRes: Int

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ) = container?.inflate(layoutRes)

    open fun onBackPressed() {}
}