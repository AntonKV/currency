package com.gmail.kolomanton.currencies.di.provider

import android.arch.persistence.room.Room
import android.content.Context
import com.gmail.kolomanton.currencies.model.data.database.CurrencyDatabase
import javax.inject.Inject
import javax.inject.Provider

class DatabaseProvider @Inject constructor(
        private val context: Context
) : Provider<CurrencyDatabase> {

    override fun get() = Room
            .databaseBuilder(
                    context,
                    CurrencyDatabase::class.java,
                    "currency.db"
            ).build()
}