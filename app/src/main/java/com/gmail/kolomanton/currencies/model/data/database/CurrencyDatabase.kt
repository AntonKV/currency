package com.gmail.kolomanton.currencies.model.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.gmail.kolomanton.currencies.entity.Currency
import com.gmail.kolomanton.currencies.model.data.database.dao.CurrencyDao

@Database(entities = [
    Currency::class
], version = 1)
abstract class CurrencyDatabase : RoomDatabase() {
    abstract fun currencyDao(): CurrencyDao
}