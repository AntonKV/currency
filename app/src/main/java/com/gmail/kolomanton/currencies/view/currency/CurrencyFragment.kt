package com.gmail.kolomanton.currencies.view.currency

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.gmail.kolomanton.currencies.R
import com.gmail.kolomanton.currencies.di.DI
import com.gmail.kolomanton.currencies.entity.Currency
import com.gmail.kolomanton.currencies.extention.toast
import com.gmail.kolomanton.currencies.extention.visible
import com.gmail.kolomanton.currencies.presenter.currency.CurrencyPresenter
import com.gmail.kolomanton.currencies.presenter.currency.CurrencyView
import com.gmail.kolomanton.currencies.view.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_currencies.*
import toothpick.Toothpick


class CurrencyFragment : BaseFragment(), CurrencyView {
    override val layoutRes = R.layout.fragment_currencies

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

    private val adapter: CurrencyAdapter by lazy {
        CurrencyAdapter(
                { presenter.onClick(it) },
                { presenter.textChange(it) },
                {
                    val layoutManager = (recyclerView.layoutManager as LinearLayoutManager)
                    val visible = layoutManager.findFirstVisibleItemPosition()
                    val offset = layoutManager.getChildAt(visible)
                    if (offset != null)
                        layoutManager.scrollToPositionWithOffset(0, offset.top)
                    else
                        recyclerView.smoothScrollToPosition(0)
                }
        )
    }

    @InjectPresenter
    lateinit var presenter: CurrencyPresenter

    @ProvidePresenter
    fun providePresenter(): CurrencyPresenter =
            Toothpick
                    .openScope(DI.SERVER_SCOPE)
                    .getInstance(CurrencyPresenter::class.java)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            itemAnimator = DefaultItemAnimator()
            setHasFixedSize(true)
            adapter = this@CurrencyFragment.adapter
        }
    }

    override fun showCurrency(show: Boolean, currency: List<Currency>) {
        recyclerView.visible(show)
        recyclerView.post { adapter.setData(currency) }
    }

    override fun multiplyCurrency(multiply: Float) {
        adapter.changeTypeSubject.onNext(multiply)
    }

    override fun showError(throwable: Throwable) {
        context!!.toast(throwable.localizedMessage)
    }
}