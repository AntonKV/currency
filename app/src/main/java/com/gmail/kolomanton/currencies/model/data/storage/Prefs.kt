package com.gmail.kolomanton.currencies.model.data.storage

import android.content.Context
import javax.inject.Inject

class Prefs @Inject constructor(
        private val context: Context
) : LastCurrencyHolder {
    private val CURRENCY = "currency"
    private val CURRENCY_ID = "currency id"
    private val CURRENCY_NUMBER = "currency number"

    private fun getSharedPreferences(prefsName: String) = context.getSharedPreferences(prefsName, Context.MODE_PRIVATE)

    override var currencyId: String
        get() = getSharedPreferences(CURRENCY).getString(CURRENCY_ID, "EUR")
        set(value) {
            getSharedPreferences(CURRENCY).edit().putString(CURRENCY_ID, value).apply()
        }

    override var currencyNumber: Float
        get() = getSharedPreferences(CURRENCY).getFloat(CURRENCY_NUMBER, 0.0f)
        set(value) {
            getSharedPreferences(CURRENCY).edit().putFloat(CURRENCY_NUMBER, value).apply()
        }
}