package com.gmail.kolomanton.currencies.model.data.server.deserializer

import com.gmail.kolomanton.currencies.entity.LatestCurrencies
import com.gmail.kolomanton.currencies.entity.Currency
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import java.lang.reflect.Type

class RatesDeserializer : JsonDeserializer<LatestCurrencies> {
    override fun deserialize(
            json: JsonElement?,
            typeOfT: Type?,
            context: JsonDeserializationContext?
    ) = if (json != null && context != null) {
        val jsonObject = json.asJsonObject
        if (!jsonObject.has("rates")) {
            LatestCurrencies("", listOf())
        } else {
            val subJsonObject = jsonObject.get("rates").asJsonObject.entrySet()
            LatestCurrencies(
                    jsonObject.get("base").asString,
                    subJsonObject.mapNotNull { Currency(it.key, it.value.asFloat) }
            )
        }
    } else {
        throw JsonParseException("RatesDeserializer. Configure Gson in GsonProvider.")
    }
}