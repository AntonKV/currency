package com.gmail.kolomanton.currencies.presenter.launch

import com.arellomobile.mvp.InjectViewState
import com.gmail.kolomanton.currencies.model.navigation.Screens
import com.gmail.kolomanton.currencies.presenter.global.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class LaunchPresenter @Inject constructor(
        private val router: Router
) : BasePresenter<LaunchView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        router.newRootScreen(Screens.CURRENCIES_SCREEN)
    }

    fun onBackPressed() = router.finishChain()
}