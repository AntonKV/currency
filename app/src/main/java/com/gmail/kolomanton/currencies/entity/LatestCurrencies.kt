package com.gmail.kolomanton.currencies.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.gmail.kolomanton.currencies.view.base.adapter.ViewType
import com.google.gson.annotations.SerializedName

data class LatestCurrencies(
        @SerializedName("base") val base: String,
        @SerializedName("rates") val currencies: List<Currency>
)

@Entity(tableName = "currency_table")
data class Currency(
        @PrimaryKey val id: String,
        var rate: Float
) : ViewType {
    @Ignore var currency: Float = 0.0f

    var position: Int = -1

    override fun getViewType() = AdapterConst.Currency.ITEM
}