package com.gmail.kolomanton.currencies.model.repository

import com.gmail.kolomanton.currencies.entity.Currency
import com.gmail.kolomanton.currencies.entity.LatestCurrencies
import com.gmail.kolomanton.currencies.model.data.database.dao.CurrencyDao
import com.gmail.kolomanton.currencies.model.data.server.CurrenciesApi
import com.gmail.kolomanton.currencies.model.data.storage.LastCurrencyHolder
import com.gmail.kolomanton.currencies.model.system.SchedulersProvider
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CurrencyRepository @Inject constructor(
        private val currenciesApi: CurrenciesApi,
        private val schedulers: SchedulersProvider,
        private val holder: LastCurrencyHolder,
        private val currencyDao: CurrencyDao
) {
    fun getCurrency(): Flowable<LatestCurrencies> = currenciesApi
            .getCurrencyList(holder.currencyId)
            .subscribeOn(schedulers.io())
            .repeatWhen { it.delay(1, TimeUnit.SECONDS) }
            .onBackpressureLatest()
            .observeOn(schedulers.ui())

    fun updateCurrencyId(currency: String) {
        holder.currencyId = currency
    }

    fun saveMoneyNumber(money: Float) {
        holder.currencyNumber = money
    }

    fun getMoneyCount() = holder.currencyNumber

    fun getCurrencyId() = holder.currencyId


    fun cacheData(latestCurrencies: LatestCurrencies) {
        val compositeDisposable = CompositeDisposable()
        compositeDisposable.add(getCacheData()
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.io())
                .subscribe { cacheData ->
                    latestCurrencies.currencies.let { newData ->
                        if (cacheData.isNotEmpty()) {
                            if (isDataDifferent(cacheData, newData)) {
                                cacheData.forEachIndexed { index, currency ->
                                    currency.position = index + 1
                                    currency.rate = newData
                                            .find { it.id == currency.id }?.rate ?: 1.0f
                                }
                                cacheData.find { it.id == latestCurrencies.base }?.position = 0
                                currencyDao.insertAll(cacheData)
                            }
                        } else {
                            newData.forEachIndexed { index, currency ->
                                currency.position = index + 1
                            }
                            val baseCurrency = Currency(latestCurrencies.base, 1.0f)
                            baseCurrency.position = 0
                            (newData as MutableList).add(baseCurrency)
                            currencyDao.insertAll(newData)
                        }
                    }
                    compositeDisposable.dispose()
                })
    }

    private fun isDataDifferent(firstList: List<Currency>, secondList: List<Currency>) =
            (firstList + secondList).groupBy { it.id }
                    .filter { it.value.size > 1 && it.value[0].rate != it.value[1].rate }
                    .flatMap { it.value }
                    .isNotEmpty()

    fun getCacheData(): Flowable<List<Currency>> = currencyDao.getAll()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())

}