package com.gmail.kolomanton.currencies.presenter.global

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BasePresenter<V : MvpView> : MvpPresenter<V>() {

    val compositeDisposable = CompositeDisposable()

    override fun detachView(view: V) {
        compositeDisposable.clear()
        super.detachView(view)
    }

    protected fun Disposable.connect() {
        compositeDisposable.add(this)
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
        super.onDestroy()
    }
}