package com.gmail.kolomanton.currencies.presenter.currency

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.gmail.kolomanton.currencies.entity.Currency

@StateStrategyType(AddToEndSingleStrategy::class)
interface CurrencyView: MvpView {
    fun showCurrency(show: Boolean, currency: List<Currency>)
    fun multiplyCurrency(multiply: Float)
    fun showError(throwable: Throwable)
}