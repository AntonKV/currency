package com.gmail.kolomanton.currencies.view.currency

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.ViewGroup
import com.gmail.kolomanton.currencies.R
import com.gmail.kolomanton.currencies.entity.Currency
import com.gmail.kolomanton.currencies.extention.inflate
import com.gmail.kolomanton.currencies.view.base.DiffCallback
import com.gmail.kolomanton.currencies.view.base.adapter.ViewType
import com.gmail.kolomanton.currencies.view.base.adapter.ViewTypeDelegateAdapter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.item_currency.view.*
import java.util.concurrent.TimeUnit


class CurrencyDelegateAdapter(
        private val clickListener: (Currency) -> Unit,
        private val textChangeListener: (Float) -> Unit,
        private val changeTypeSubject: BehaviorSubject<Float>
) : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(
            parent: ViewGroup
    ): RecyclerView.ViewHolder = GalleryItemViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType?, size: Int) {
        holder as GalleryItemViewHolder
        holder.bind(item as Currency)
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        holder as GalleryItemViewHolder
        holder.recycled()
    }

    override fun onUpdateViewHolder(
            holder: RecyclerView.ViewHolder,
            item: ViewType,
            payloads: MutableList<Any>) {
        holder as GalleryItemViewHolder
        holder.update(item as Currency, payloads)
    }

    inner class GalleryItemViewHolder(parent: ViewGroup)
        : RecyclerView.ViewHolder(parent.inflate(R.layout.item_currency)) {
        private val compositeDisposable = CompositeDisposable()

        fun bind(item: Currency) {
            with(itemView) {
                country_name.text = item.id
                rate.text = Editable.Factory.getInstance()
                        .newEditable(String.format("%(.2f", item.currency))

                if (item.position != 0) {
                    compositeDisposable.add(changeTypeSubject.subscribe {
                        item.currency = item.rate * it
                        rate.text = Editable.Factory.getInstance()
                                .newEditable(String.format("%(.2f", item.currency))
                    })
                    setOnClickListener {
                        compositeDisposable.clear()
                        clickListener(item)
                    }
                } else {
                    compositeDisposable.add(
                            Observable.create<String> { emitter ->
                                rate.addTextChangedListener(object : TextWatcher {
                                    override fun beforeTextChanged(charSequence: CharSequence,
                                                                   i: Int, i1: Int, i2: Int) {
                                    }

                                    override fun onTextChanged(charSequence: CharSequence,
                                                               i: Int, i1: Int, i2: Int) {
                                    }

                                    override fun afterTextChanged(editable: Editable) {
                                        if (!emitter.isDisposed) {
                                            emitter.onNext(editable.toString())
                                        }
                                    }
                                })
                            }
                                    .subscribeOn(Schedulers.io())
                                    .debounce(300, TimeUnit.MILLISECONDS)
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe { money ->
                                        val newCurrency = if (money != null && money.isNotEmpty())
                                            money.toFloat()
                                        else
                                            0.0f
                                        item.currency = newCurrency
                                        textChangeListener(newCurrency)
                                    }
                    )
                }

                setEditTextFocusable(item.position == 0)
            }
        }

        fun recycled() {
            compositeDisposable.clear()
        }

        fun update(item: Currency, payloads: MutableList<Any>) {
            payloads.forEach {
                if (it is Bundle
                        && it.containsKey(DiffCallback.NEW_ITEM_POSITION)
                        && it.containsKey(DiffCallback.OLD_ITEM_POSITION)) {
                    val newItemPosition = it.getInt(DiffCallback.NEW_ITEM_POSITION)
                    val oldItemPosition = it.getInt(DiffCallback.OLD_ITEM_POSITION)
                    if (newItemPosition == 0) {
                        setEditTextFocusable(true)
                        compositeDisposable.clear()
                        itemView.rate.text = Editable.Factory.getInstance()
                                .newEditable(String.format("%(.2f", item.currency))
                        itemView.setOnClickListener { }

                        compositeDisposable.add(
                                Observable.create<String> { emitter ->
                                    itemView.rate.addTextChangedListener(object : TextWatcher {
                                        override fun beforeTextChanged(charSequence: CharSequence,
                                                                       i: Int, i1: Int, i2: Int) {
                                        }

                                        override fun onTextChanged(charSequence: CharSequence,
                                                                   i: Int, i1: Int, i2: Int) {
                                        }

                                        override fun afterTextChanged(editable: Editable) {
                                            if (!emitter.isDisposed) {
                                                emitter.onNext(editable.toString())
                                            }
                                        }
                                    })
                                }
                                        .subscribeOn(Schedulers.io())
                                        .debounce(300, TimeUnit.MILLISECONDS)
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe { money ->
                                            val newCurrency = if (money != null && money.isNotEmpty())
                                                money.toFloat()
                                            else
                                                0.0f
                                            item.currency = newCurrency
                                            textChangeListener(newCurrency)
                                        }
                        )
                    }

                    if (oldItemPosition == 0) {
                        setEditTextFocusable(false)
                        compositeDisposable.clear()
                        compositeDisposable.add(changeTypeSubject.subscribe {
                            item.currency = item.rate * it
                            itemView.rate.text = Editable.Factory.getInstance()
                                    .newEditable(String.format("%(.2f", item.currency))
                        })
                        itemView.setOnClickListener { clickListener(item) }
                    }
                }
            }
        }

        private fun setEditTextFocusable(isInFocus: Boolean) {
            itemView.rate.isFocusable = isInFocus
            itemView.rate.isFocusableInTouchMode = isInFocus
        }
    }
}