package com.gmail.kolomanton.currencies.di.module

import android.content.Context
import com.gmail.kolomanton.currencies.di.provider.CurrencyDaoProvider
import com.gmail.kolomanton.currencies.di.provider.DatabaseProvider
import com.gmail.kolomanton.currencies.model.data.database.CurrencyDatabase
import com.gmail.kolomanton.currencies.model.data.database.dao.CurrencyDao
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import toothpick.config.Module

class AppModule(context: Context) : Module() {
    init {
        //Global
        bind(Context::class.java).toInstance(context)

        //Navigation
        val cicerone = Cicerone.create(Router())
        bind(Router::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)

        //Database
        bind(CurrencyDatabase::class.java)
                .toProvider(DatabaseProvider::class.java).providesSingletonInScope()
        bind(CurrencyDao::class.java)
                .toProvider(CurrencyDaoProvider::class.java).providesSingletonInScope()

    }
}