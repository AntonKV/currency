package com.gmail.kolomanton.currencies.view.base

import android.os.Bundle
import android.support.v7.util.DiffUtil
import com.gmail.kolomanton.currencies.entity.Currency

class DiffCallback(
        private val newData: List<Currency>,
        private val oldData: List<Currency>
) : DiffUtil.Callback() {
    override fun getOldListSize() = oldData.size
    override fun getNewListSize() = newData.size

    override fun areItemsTheSame(
            oldItemPosition: Int,
            newItemPosition: Int
    ): Boolean = oldData[oldItemPosition].id == newData[newItemPosition].id


    override fun areContentsTheSame(
            oldItemPosition: Int,
            newItemPosition: Int
    ): Boolean = oldData[oldItemPosition].rate == newData[newItemPosition].rate

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int) = Bundle().apply {
        putInt(OLD_ITEM_POSITION, oldItemPosition)
        putInt(NEW_ITEM_POSITION, newItemPosition)
    }

    companion object {
        const val OLD_ITEM_POSITION = "oldItemPosition"
        const val NEW_ITEM_POSITION = "newItemPosition"
    }
}