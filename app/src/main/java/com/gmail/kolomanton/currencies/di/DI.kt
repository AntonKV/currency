package com.gmail.kolomanton.currencies.di

object DI {
    const val APP_SCOPE = "app scope"
    const val SERVER_SCOPE = "server scope"
}