package com.gmail.kolomanton.currencies.di.provider

import com.gmail.kolomanton.currencies.entity.LatestCurrencies
import com.gmail.kolomanton.currencies.model.data.server.deserializer.RatesDeserializer
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import javax.inject.Inject
import javax.inject.Provider

class GsonProvider @Inject constructor() : Provider<Gson> {

    override fun get(): Gson =
            GsonBuilder()
                    .registerTypeAdapter(LatestCurrencies::class.java, RatesDeserializer())
                    .create()
}