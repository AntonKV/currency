package com.gmail.kolomanton.currencies.model.data.server

import com.gmail.kolomanton.currencies.entity.LatestCurrencies
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrenciesApi {
    @GET("latest")
    fun getCurrencyList(
            @Query("base") baseCurrencies : String
    ): Single<LatestCurrencies>
}